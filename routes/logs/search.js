/*
 * url: /search
 * 搜寻
 */
const DB    = require(SYS_CONF.path + '/model/db_logs');
const User_DB    = require(SYS_CONF.path + '/model/db_user');
const SQL   = require(SYS_CONF.path + '/lib/sql');
const Func  = require(SYS_CONF.path + '/lib/func');
const Plus  = require(SYS_CONF.path + '/lib/plus');

async function search(ctx){
        let d = await Plus.query(ctx);
        let ret = Plus.ret({status:1});
        let uid = ctx.session ? (ctx.session.userinfo ? ctx.session.userinfo.id : 0) : 0;

        let wer = uid ? "where uid="+uid : "";

        //按项目名搜索
        if(d.action) wer = (wer ? "" : " where ") + ' action ' + SQL.like(d.action);

        let dats = await DB.search( wer+ ' order by id desc', '', d);
        //console.log(dats);
        if(dats.data.length > 0){
            let raw = await User_DB.list();
            let user_list = {};
            raw.forEach(function(v,i){ user_list[v.id] = v.real_name; });
            dats.data.forEach(function(v,i){ v.owner = user_list[v.uid]; });
       }

        ret.total = dats.total;
        ret.data = dats.data;
        //ret.list = await User_DB.list();

        return ret;
};

module.exports = async (ctx, next) => {
    ctx.body = await search(ctx);
 };
