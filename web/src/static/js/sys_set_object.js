/*系统设置*/
let Sys_set_object = {
   "field": function(n,t){
      return Apps.field("sys_set", n, t);
   },
   "show": function(){
      let node = $("#sys_set");
      let is_tunnel = node.find('input[name="is_tunnel"]')[0];
      let obj = this.field([
        'host',
        'port',
        'user',
        'password',
        'database',
        'keys',
        'tunnel_host',
        'tunnel_port',
        'tunnel_user',
        'tunnel_password',
        'service_port',
        'public_key',
        'private_key'
      ]);

      $.each(obj, function(k,v){ v.val(window.SYS_CONF.db[k]); });

      is_tunnel.checked = window.SYS_CONF.db.is_tunnel;

      let opt = Apps.dialog_opt();
      opt.title = "系统设置";
      opt.buttons = [
         {text:"确认",iconCls :'icon-ok',handler:function(){
            let dats = {};
            let cip = {"ciphertext":""};
            $.each(obj, function(k,v){ dats[k] = v.val(); });
            dats.is_tunnel = is_tunnel.checked;

            cip.ciphertext = Enc.encrypt(JSON.stringify(dats), window.SYS_CONF.user_token);

            //console.log(cip);
            //back
            Fs.writeFileSync(window.SYS_CONF.config_file + '_bak', Fs.readFileSync(SYS_CONF.config_file));
            //update
            Fs.writeFileSync(window.SYS_CONF.config_file, JSON.stringify(cip));
            node.dialog('close');
        }},
        {text:"取消",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}
      ];
      $("#sys_set_tabs").tabs({tabHeight:25});
      Apps.Dialog_Show(node, opt);
   }
};
