//帐号列表
let Dbs_Object = {
   "field": function(n,t){ return Apps.field("dbs_info", n, t); },
   "reload": function(){ this.list(); },
   "menu": function(e, i, r){
       e.preventDefault();//阻止浏览器自带的右键菜单弹出
       let m = $('#dbs_menu');

       let dats = $(this).datagrid("getSelections");
       if(dats.length < 1){ $(this).datagrid('selectRow',i); }

       if(m.children("div:not([class*='menu-line'])").length > 0){
          m.menu('show', {left:e.pageX, top:e.pageY, hideOnUnhover:false});
       }
   },
   "list": function(page, limit){
         let dg = $("#dbs_dg");

         let dat ={
             page: (page ? page : 0),
             limit: (limit ? limit : null)
         };
         Public_Object.search(dg, dat, {
             "api_url": window.Api.dbs.search,
             "callback":function(d){
                     let kinds = {"0":"MySQL","1":"Mssql","2":"sqlite"};
                     let projects = {};
                     window.Apps.data.project.forEach(function(v,i){
                           projects[v.id.toString()] = v.project_name;
                     });

                     d.forEach(function(v,i){
                             v.kind_name = kinds[v.kind];
                             v.project_name = projects[v.project_id];
                             v.create_date = UnixToDateTime(v.create_date);
                             v.status_val= v.status ? '<font color=green>正常</font>' : '<font color=red>已禁用</font>';
                     });
                     return d;
             }
         }, Dbs_Object.list);

   },
   "row_dbclick":function(rowIndex, rowData){ Dbs_Object.change(); },
   "change": function(act){
      let node = $("#dbs_info");
      let obj = this.field(['milieu_id','project_id', 'kind', 'host', 'port','user', 'passwd','dbname','create_date','status']);
      let kinds = [{"id":0,"kind_name":"MySQL"},{"id":1,"kind_name":"Mssql"},{"id":2,"kind_name":"sqlite"}];
      let row = null;
      $.each(obj, function(i,v){ v.val(''); });

      obj.milieu_id.find('option:not(:first-child)').remove();
      window.Apps.data.milieu.forEach(function(v,i){
             obj.milieu_id.append('<option value="'+v.id+'">'+v.milieu_name+'</option>');
      });
      obj.project_id.find('option:not(:first-child)').remove();
      window.Apps.data.project.forEach(function(v,i){
             obj.project_id.append('<option value="'+v.id+'">'+v.project_name+'</option>');
      });
      obj.kind.find('option:not(:first-child)').remove();
      kinds.forEach(function(v,i){
             obj.kind.append('<option value="'+v.id+'">'+v.kind_name+'</option>');
      });


      if(!act){
         //修改
         row = $("#dbs_dg").datagrid("getSelected");
         if(!row) return;
         Object.keys(obj).forEach(function(k){ obj[k].val(row[k]); });
         //obj.create_date.val(UnixToDateTime(row.create_date));
         obj.status[0].checked = row.status ? true : false;
      }

      let opt = Apps.dialog_opt();
      opt.title = act ? "新增帐号" : "修改帐号信息 ID: " + row.id;
      opt.buttons = [
         {text:"确认",iconCls :'icon-ok',handler:function(){
             let dats = {
                "id":act ? 0 : row.id,
                "milieu_id":obj.milieu_id.val(),
                "project_id":obj.project_id.val(),
                "kind":obj.kind.val(),
                "host":obj.host.val(),
                "port":obj.port.val(),
                "user":obj.user.val(),
                "passwd":obj.passwd.val(),
                "dbname":obj.dbname.val(),
                "status":(obj.status[0].checked ? 1 : 0)
             };

             let api_url = window.Api.dbs.add;

             if(!act){
                  if(dats.user_name) delete dats.user_name;
                  api_url = window.Api.dbs.update;
             }
             get_dats(api_url, dats, function(d,err){
                     if(err){console.log(err); alert("保存出错!"); return;}
                     if(!d.status){Public_Object.alert(d.msg); return;}
                     //console.log(sql);
                     Dbs_Object.reload();
             });

   	      	node.dialog('close');
   	    }}, {text:"取消",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}
      ];
      Apps.Dialog_Show(node, opt);
   },
   "del": function(){
      let row = $("#dbs_dg").datagrid("getSelected");
      if(!row) return;
      Public_Object.confirm({content:'<p>您确定要删除 "'+row.id+'"</p>'}, function(){
          let ret = getByRsync(window.Api.dbs.del,{"id":row.id});
          if(!ret.status) Public_Object.alert(ret.msg);
          Dbs_Object.reload();
      });
   },
}
