/*公共模块*/
let Public_Object = {
   "confirm": function(d,c){
      let o = d ? d : {};
      let node = $('<div class="alert"></div>');
      let opt = Apps.dialog_opt();
      node.html( o.content ? o.content : '' );
      opt.title = o.title ? o.title : "提示";
      opt.buttons = [
         {text:"确认",iconCls :'icon-ok',handler:function(){
            if(c) c();
            node.dialog('destroy');
        }},
        {text:"取消",iconCls :'icon-cancel',handler:function(){ node.dialog('destroy'); }}
      ];
      Apps.Dialog_Show(node, opt);
   },
   "alert": function(s,t){
      let node = $('<div class="alert"></div>');
      let opt = Apps.dialog_opt();
      node.html( s ? s : '' );
      opt.title = t ? t : "提示";
      opt.buttons = [ {text:"确认",iconCls :'icon-ok',handler:function(){ node.dialog('destroy'); }} ];
      Apps.Dialog_Show(node, opt);
   },
   "list": {
     "create_option" : function(o){
               if('object' != typeof(o)){console.log("Error from Customer_Object.create_option !"); return;}
               let def = {
                 dats : o.dats,
                 obj  : o.obj,
                 id   : o.id,
                 text : o.text,
                 callback : o.callback ? o.callback : null
               };
               def.obj.find("option:not(:first-child)").remove();
               $.each(def.dats, function(i, v){
                 let value = def.id ? v[def.id] : i.toString();
                 let txt = def.text ? v[def.text] : v;
                 def.obj.append('<option value="'+value+'">'+txt+'</option>');
               });
               if(def.callback) def.callback();
      },
      "accounts_kind_list" : function(o, ob, c){
      			let arr = [];
      			for(let attr in ob){
      				arr.push(ob[attr])
      			};
      			arr = arr.sort(function(a,b){return a.show_index - b.show_index})
              this.create_option({
                dats  : arr,
                obj   : o,
                id    : "id",
                text  : "kind_name",
                callback : c
              });
      }
    },
    "search": function(dg, data, obj, getData){
            if('object' != typeof(dg)){ Public_Object.alert("未传入表格对象!"); return; }
            let api_url = obj ? obj.api_url  : "";

            dg.datagrid("loading"); //加屏蔽

            get_dats(api_url, data, function(d, err){
                 if(err){ Public_Object.alert("获取列表返回错误!"); return; }
                 if(!d.status){dg.datagrid("loaded"); Public_Object.alert(d.msg);}

                 if(d.data){
                       Public_Object.pagerFilter(getData, dg, d.total, d.data.length); //换页设置
                       let cdata = obj.callback ? obj.callback(d.data) : null;
                       if(!cdata) cdata = d.data;

                       //加载数据
                       dg.datagrid('loadData',{rows:cdata, total:(d.total ? d.total : 0) });
                       dg.datagrid("loaded"); //移除屏蔽
                 }
            });
    },
    "pagerFilter": function(getData, dg, total, rows ){
           if(!getData) return;
           let opts = dg.datagrid('options');
           let pager = dg.datagrid('getPager');

          pager.pagination({
              "total": total,
              "rows": rows,
              onSelectPage:function(pageNum, pageSize){
                $('.pagination-page-list_input').hide();
                //let pNum = (pageNum > 0) ? (pageNum - 1 ) : 0;
                opts.pageSize = pageSize;
                getData(pageNum, pageSize);
              }
          });
   },
    /* 提交修改 -- 同步  提交请求会 等待返回  */
    "get_data": function(api, dats, met){
         let res;
         let result = $.ajax({
            url      : api,
            type     : met ? met : "POST",
            data     : dats,
            dataType : "json",
            cache    : false,
            async    : false,
            success  : function(data){ res = data; },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               Public_Object.tip('<p style="color:red;font-weight:bold;">无法正常访问接口!</p>');
            }
         });
         return res;
    },
    /*sys_tip*/
    "tip": function(txt){
         let tip = $(".sys_tip").eq(0);
         tip.html(txt);
         tip.fadeIn(200);
         setTimeout(function(){
            tip.fadeOut(2000);
         },3800);
    }
};
