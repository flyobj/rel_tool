//发布代码
const Release_Object = {
   "field": function(){ return Apps.field("release", ['project_id','milieu_id']); },
   "init": function(id){
          let obj = this.field();
          obj.project_id.find('option:not(:first-child)').remove();
          obj.milieu_id.find('option:not(:first-child)').remove();
          window.Apps.data.project.forEach(function(v,i){ obj.project_id.append('<option value="'+v.id+'">'+v.project_name+'</option>'); });
          window.Apps.data.milieu.forEach(function(v,i){ obj.milieu_id.append('<option value="'+v.id+'">'+v.milieu_name+'</option>'); });

   },
   "go":function(){
           let dom = $("#release");
           let obj = this.field();
           let project_id = parseInt(obj.project_id.val());
           let milieu_id = parseInt(obj.milieu_id.val());
           if(!project_id){ Public_Object.alert("请选择项目!"); return; }
           if(!milieu_id){ Public_Object.alert("请选择环境!"); return; }

           let data = {"project_id":project_id,"milieu_id":milieu_id};
           //console.log(data);

           dom.find("button").attr("disabled", true);
           dom.find("button").text("发布中...");
           obj.project_id.attr("disabled",true);
           obj.milieu_id.attr("disabled",true);

           window.Wsock.send(dom.find("div.console"), "release", data, function(){
                obj.project_id.removeAttr("disabled");
                obj.milieu_id.removeAttr("disabled");
                dom.find("button").removeAttr("disabled");
                dom.find("button").text("发布");
           });

   }
};
