/*项目类*/
let Project_Object = {
   "o": function(){
       let dom = $("#projects > div.f_list");
       return {
          "left":dom.find(" > div:first-child > div"),
          "right":dom.find(" > div:last-child > div")
      };
   },
   "list": function(){
         get_dats(window.Api.project.search,{},function(dats){
                 if(!dats) return;
                 if(dats.status){
                     let obj = Project_Object.o().left;
                     Project_Object.o().right.find(" > ul").remove();
                     obj.find(" > ul").remove();
                     dats.data.forEach(function(v, i){
                            let ul = $('<ul></ul>');
                            let li = $('<li></li>');
                            li.text(v.project_name);
                            if(!v.status) li.css('color','#555');
                            ul.append(li);
                            ul.data("raw",v);
                            ul.click(function(){ Project_Object.select($(this)); });   //单击，查看列表
                            ul.dblclick(function(){ Project_Object.change($(this).data("raw")); });   //双击查看项目信息
                            obj.append(ul);
                     });
                 } else {
                     Public_Object.alert(dats.msg);
                 }
         });
   },
   "select": function(dom){
          dom.parent().find("ul").removeClass("sel");
          dom.addClass("sel");
          let raw = dom.data("raw");
          this.node_list(raw.id);
   },
   "node_change": function(row){
         let node = $("#add_project_node");
         let obj = Apps.field(node[0], ['milieu_id','project_id','rsync_user','rsync_passwd','rsync_name','nodes','shell_path','create_date']);
         let status = node.find("input[name='status']").parent().parent();
         let is_sync = node.find("input[name='is_sync']").parent().parent();
         status.find("input").removeAttr("checked");
         is_sync.find("input").removeAttr("checked");

         obj.milieu_id.find("option:not(:first-child)").remove();
         obj.project_id.find("option:not(:first-child)").remove();
         window.Apps.data.milieu.forEach(function(v,i){
               obj.milieu_id.append('<option value="'+v.id+'">'+v.milieu_name+'</option>');
         });
         window.Apps.data.project.forEach(function(v,i){
               obj.project_id.append('<option value="'+v.id+'">'+v.project_name+'</option>');
         });

         if(row){
               if(!row){ Project_Object.alert('未选择记录!'); return; }
               $.each(obj, function(i,v){ v.val(row[i]); });
               if(row.is_sync){ is_sync.find("input[value='1']")[0].checked = true; }else{ is_sync.find("input[value='0']")[0].checked = true; }
               if(row.status){ status.find("input[value='1']")[0].checked = true; }else{ status.find("input[value='0']")[0].checked = true; }
               obj.create_date.val(UnixToDateTime(row.create_date));
         } else {
                $.each(obj, function(i,v){ v.val(''); });
                let raw = Project_Object.o().left.find('> ul.sel').data('raw');
                obj.project_id.val(raw ? raw.id : '');
        }

        let opt = Apps.dialog_opt();
        opt.title = row ? "发布节点信息修改 ID: " + row.id : "新增发布节点";
        opt.buttons = [
           {text:"确认",iconCls :'icon-ok',handler:function(){
               let dats = {
                  "milieu_id":obj.milieu_id.val(),
                  "project_id":obj.project_id.val(),
                  "rsync_user":obj.rsync_user.val(),
                  "rsync_passwd":obj.rsync_passwd.val(),
                  "rsync_name":obj.rsync_name.val(),
                  "shell_path":obj.shell_path.val(),
                  "is_sync":node.find('input[name="is_sync"]:checked').val(),
                  "status":node.find('input[name="status"]:checked').val(),
                  "nodes":obj.nodes.val()
               };
               if(row) dats.id = row.id;
               let api_url = row ? window.Api.project.node_update : window.Api.project.node_add;

               get_dats(api_url,dats,function(d){
                      if(!d.status){Project_Object.alert("发生错误: " + d.msg); return;}
                      Project_Object.node_list(dats.project_id);
               });
               node.dialog('close');
          }},{text:"取消",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}
        ];
        Apps.Dialog_Show(node, opt);
   },
   "change": function(row){
         let node = $("#add_project");
         let obj = Apps.field(node[0], ['project_name','exclude','note','create_date']);
         let status = node.find("input[type='radio']").parent().parent();
         status.find("input").removeAttr("checked");

         if(row){
               if(!row){ Project_Object.alert('未选择记录!'); return; }
               $.each(obj, function(i,v){ v.val(row[i]); });
               if(row.status){ status.find("input[value='1']")[0].checked = true; }else{ status.find("input[value='0']")[0].checked = true; }
               obj.create_date.val(UnixToDateTime(row.create_date));
         } else {
                $.each(obj, function(i,v){ v.val(''); });
        }

         let opt = Apps.dialog_opt();
         opt.title = row ? "项目信息修改 ID: " + row.id : "新增项目";
         opt.buttons = [
            {text:"确认",iconCls :'icon-ok',handler:function(){
                let dats = {
                   "project_name":obj.project_name.val(),
                   "exclude":obj.exclude.val(),
                   "status":node.find('input[type="radio"]:checked').val(),
                   "note":obj.note.val()
                };
                if(row) dats.id = row.id;

                let api_url = row ? window.Api.project.update : window.Api.project.add;

                get_dats(api_url,dats,function(d){
                       if(!d.status){
                            Project_Object.alert("发生错误: " + d.msg);
                       }else {
                            window.Apps.data.project = d.data;
                       }
                       Project_Object.list();
                });
                node.dialog('close');
           }},
            {text:"取消",iconCls :'icon-cancel',handler:function(){
                node.dialog('close');
           }}
         ];
         Apps.Dialog_Show(node, opt);
   },
   "del": function(){
         let row = this.o(1).data("raw");
         if(row){
           Public_Object.confirm({content:'<p>您确定要删除客户 "'+row.customer_name+'"</p>'}, function(){
               db.project.del(row.id, function(err){
                  if(err) Project_Object.alert("操作失败!");
                  Project_Object.list();
               });
           });
         } else {
            Project_Object.alert("未选择记录!");
         }
   },
   "node_list": function(id){
           let projects = {};
           let milieus = {};
           window.Apps.data.project.forEach(function(v,i){projects[v.id] = v.project_name; });
           window.Apps.data.milieu.forEach(function(v,i){milieus[v.id] = v.milieu_name; });
           get_dats(window.Api.project.node,{"project_id":id},function(dats){
                   if(!dats) return;
                   if(dats.status){
                       let obj = Project_Object.o().right;
                       obj.find(" > ul").remove();
                       dats.data.forEach(function(v, i){
                              let ul = $('<ul><li>'+milieus[v.milieu_id]+'</li><li>'+projects[v.project_id]+'</li><li>'+v.nodes.replace(/\n/g,', ')+'</li></ul>');
                              ul.data("raw",v);
                              ul.click(function(){
                                   dom = $(this);
                                   dom.parent().find("ul").removeClass("sel");
                                   dom.addClass("sel");
                              });   //点击选中
                              ul.dblclick(function(){ Project_Object.node_change($(this).data("raw"));});
                              obj.append(ul);
                       });
                   }
           });
   }
};
