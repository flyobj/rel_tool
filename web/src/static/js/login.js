let Login_Object = {
   "dom": function(){
        return {
          "user_name": $("#user_name"),
          "password": $("#password"),
          "verif_code": $("#verif_code")
        }
   },
   "init": function(){
       let dom = this.dom();
       dom.user_name.bind('keypress',function(event){
         if(event.keyCode == 13) $("#password").focus();
       });

       dom.password.bind('keypress',function(event){
         if(event.keyCode == 13) $("#verif_code").focus();
       });

       dom.verif_code.bind('keypress',function(event){
          if(event.keyCode == 13){
            Login_Object.login();
            $(this).blur();
          }
       });
   },
   "reset": function(){
        let dom = this.dom();
        dom.user_name.val('');
        dom.password.val('');
   },
   "input_check": function(){
      let dom = this.dom();
      let ret = {status:0, data:{}, msg:""};
      if(dom.user_name.val().length < 1){
        ret.msg = "请输入用户名!";
        return ret;
      }
      if(dom.password.val().length < 1){
        ret.msg = "请输入密码!";
        return ret;
      }
      ret.status = 1;
      ret.data = {
         "user_name":dom.user_name.val(),
         "password":Apps.pwd(dom.user_name.val(), dom.password.val()),
         "verif_code":dom.verif_code.val()
      };
      return ret;
   },
   "login": function(){
         let res = this.input_check();
         if(!res.status){
           Public_Object.alert(res.msg);
           return 0;
         }

         //console.log(res);

         get_dats("/api.user/login",res.data, function(dats){
              //console.log(dats);
              if(dats.status){
                   Login_Object.login_ok(dats);
              } else {
                  Login_Object.login_error(dats.msg);
                  $("#verif_code").next().attr('src','/api.verif?t='+Math.random());
              }

         });
   },
   "login_ok": function(dats){
        window.Apps.me = dats.me;
        window.Apps.data = dats.data;

        //执行权限检查
        window.Apps.check_grants();

        //初始化
        app_init();
        $("body > .LoginWin").remove();
        $("body > .main").show();
   },
   "login_error": function(msg){
       $.messager.alert("登陆失败",(msg ? msg : "帐号或密码错误!"),"error", function(){
            let pwd = Login_Object.dom().password;
            pwd.select();
            pwd.focus();
       });
   },
   "check_port": function(port, callback){
        let server = require('net').createServer().listen(port)
        server.on('listening',function(){
          server.close();
          if(callback) callback();
        });
        server.on('error',function(err){
            if(err.code === 'EADDRINUSE' && callback) callback(err);
        });
   }
}
