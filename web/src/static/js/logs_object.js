//帐号列表
let Logs_Object = {
   "field": function(n,t){ return Apps.field("logs_info", n, t); },
   "reload": function(){ this.list(); },
   "menu": function(e, i, r){
       e.preventDefault();//阻止浏览器自带的右键菜单弹出
       let m = $('#logs_menu');

       let dats = $(this).datagrid("getSelections");
       if(dats.length < 1){ $(this).datagrid('selectRow',i); }

       if(m.children("div:not([class*='menu-line'])").length > 0){
          m.menu('show', {left:e.pageX, top:e.pageY, hideOnUnhover:false});
       }
   },
   "list": function(page, limit){
         let dg = $("#logs_dg");
         let dat ={
             page: (page ? page : 0),
             limit: (limit ? limit : null)
         };
         Public_Object.search(dg, dat, {
             "api_url": window.Api.logs,
             "callback":function(d){
                     d.forEach(function(v,i){
                           v.create_date = UnixToDateTime(v.create_date);
                     });
                     return d;
             }
         }, Logs_Object.list);
   },
   "info": function(){
        let node = $("#logs_info");
        let obj = Logs_Object.field(['action','val', 'ip', 'note', 'owner', 'create_date']);
        let row = null;
        $.each(obj, function(i,v){ v.val(''); });

         //修改
         row = $("#logs_dg").datagrid("getSelected");
         if(!row) return;
         Object.keys(obj).forEach(function(k){ obj[k].val(row[k]); });

        let opt = Apps.dialog_opt();
        opt.title = "日志 ID: " + row.id;
        opt.buttons = [{text:"关闭",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}];
        Apps.Dialog_Show(node, opt);
   }
}
