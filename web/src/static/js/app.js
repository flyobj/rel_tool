window.Apps = {
  "server" : {
     "protocol": window.location.protocol,
     "host": window.location.hostname,
     "port": window.location.port,
     "url" : function(){
       return this.protocol+"//"+this.host+":"+this.port;
     },
  },
  "pwd": function(u, p){
      let ret = "";
      if(u && p){ret = Crypt_hash(u + Crypt_hash(p, 'md5'));}
      return ret;
  },
  "dialog_opt" : function(){
    return {
        title     : '',
        closed    : false,
        cache     : false,
        top       : "23%",
        resizable : false,
        modal     : true,
        buttons   : [],
    };
  },
  "Dialog_Show": function(o, p, callback){
    o.dialog(p);
    //高度超出屏幕则顶部间距5px，否则居中
    if(o.height() < (document.documentElement.clientHeight - 30)){
        o.window('center'); //使Dialog居中显示
    } else {
        o.dialog({"top":'5px'});
    }
    if(callback) callback();
  },
  "field": function(dom, n, t){
     let node = 'string' == typeof(dom) ? $('#'+dom) : $(dom);
     let d = null;
     let type = (t ? t : '*' );
     if('object' == typeof(n)){
         d = {};
         $.each(n, function(i,v){
             d[v] = node.find(type + '[name="' + v + '"]').eq(0);
         });
     } else {
         d = node.find(type + '[name="' + n + '"]').eq(0);
     }
     return d;
  },
  "init": function(){
        //检测登陆状态
        get_dats(window.Api.user.check,{}, function(dats){

           if(dats.status){
               Login_Object.login_ok(dats);
           } else {
              $("body > .LoginWin").show();
           }

        });
  },
  //检查权限
  "check_grants": function(){
        if(!window.Apps.me.role_id) return;
        let ids = ','+window.Apps.data.role[window.Apps.me.role_id].grants.toString() + ',';
        let nav = $("div.main > div.top > p > a");
        for(let i=0; i<nav.length;i++){
               let v = nav.eq(i);
               if(!v.attr("grant_id")) continue;
               if(ids.indexOf(','+v.attr("grant_id")+',') > -1){
                       v.show();
               } else {
                      v.remove();
               }
        }
  }
};

window.Wsock = {
     "msg":function(cons, msg, color){
           //let cons = $("#run_console");
           if('DIV' === cons[0].tagName){
                 if(color){
                       let str = $('<p style="font-weight:bold;color:'+color+';"></p>');
                       str.text(msg);
                       cons.append(str);
                 }else{ cons.append(msg.replace(/\n/g,'<br/>')); }
            } else {
                  //cons.append(msg);
                  cons[0].appendChild(document.createTextNode(msg));
            }

           //设置滚动条到最底部
           if(cons[0].scrollHeight > cons[0].clientHeight) { cons[0].scrollTop = cons[0].scrollHeight; }
     },
     "send":function(dom, cmd, dat, c){
             let ws = new WebSocket((window.Apps.data.ws.is_https ? "wss://" : "ws://")+window.location.hostname+":"+window.Apps.data.ws.port);
             ws.show = this.msg;
             ws.onopen = function(evt){
                    //ws.show(dom,'\n链接成功~!\n','green');
                    if(1==this.readyState){
                         ws.show(dom,'\n-----------------------------------------[' + getLocalTime() + ']------------------------------------\n','green');
                         let data = {"cmd": cmd, "uid":window.Apps.me.id, "token":window.Apps.data.token, "data": dat};
                         ws.send(JSON.stringify(data));
                    }else{ws.show(dom,'\n-------------> 链接出错socket状态无效~!\n','red');}
            };
            //接收消息
            ws.onmessage =function(e){
                  let d = isJson(e.data);
                  if(!d){ ws.show(dom,'\n-------------> 收到未知回应消息!\n','red'); console.log("未知消息: ", e.data); return; }
                  if(!d.status){ ws.show(dom,d.msg,'yellow'); return; }
                  //ws.show(dom,JSON.stringify(d));
                  ws.show(dom,d.result);
            };
            //链接错误
            ws.onerror = function(e){ ws.show(dom,'\n-------------> 发生错误~!\n','red'); if(c)c(); };
            //链接关闭事件
            ws.onclose = function(e){ ws.show(dom,'\n--------------------------------------------------done--------------------------------------------\n\n','yellow'); if(c)c(); };
     }
};

//api地址
window.Api = {
  //用户管理
  "user": {
    "search":       "/api.user/search",
    "add":          "/api.user/add",
    "update":       "/api.user/update",
    "del":          "/api.user/del",
    "check":        "/api.user/check",
    "change_pwd":   "/api.user/change_pwd",
    "set_pwd":      "/api.user/set_pwd",
    "logout":       "/api.user/logout"
  },
  //权限管理
  "role":{
    "search":       "/api.role/search",
    "add":          "/api.role/add",
    "update":       "/api.role/update",
    "del":          "/api.role/del"
  },
  //数据库配置
  "dbs":{
     "search":    "/api.dbs/search",
     "add":          "/api.dbs/add",
     "update":   "/api.dbs/update",
     "del":            "/api.dbs/del"
  },
  //项目管理
  "project":{
    "search":               "/api.project/search",
    "add":                     "/api.project/add",
    "update":              "/api.project/update",
    "del":                       "/api.project/del",
    "node":                   "/api.project/node",
    "node_add":         "/api.project/node_add",
    "node_del":           "/api.project/node_del",
    "node_update":  "/api.project/node_update"
  },
  //执行sql任务
  "sql":{
    "search":        "/api.sql",
    "add":             "/api.sql/add",
    "update":       "/api.sql/update",
    "del":               "/api.sql/del",
    "run":              "/api.sql/run",
    "history":       "/api.sql/history"
  },
  //发布代码
  "release":"/api.release",
  //日志信息
  "logs":"/api.logs",
  //获取上传图片
  "getfile":"/api.getfile",
  "global":{
     "bank":       "/api.global/bank"
  }

};
