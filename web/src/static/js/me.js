const Me_Object = {
      "info": function(){
                let node = $("#myinfo");
                let obj = Apps.field("myinfo_tabs",['real_name','user_name', 'last_time']);
                let me = window.Apps.me;

                 obj.user_name.val(me.user_name);
                 obj.real_name.val(me.real_name);
                 obj.last_time.val(UnixToDateTime(me.last_time));

                let opt = Apps.dialog_opt();
                opt.title =  "个人信息";
                opt.buttons = [/*
                   {text:"修改",iconCls :'icon-ok',handler:function(){
                       let dats = {
                          "id":me.id,
                          "real_name":obj.real_name.val()
                       };

                       let api_url = window.Api.user.update;

                       get_dats(api_url, dats, function(d,err){
                               if(err){console.log(err); alert("保存出错!"); return;}
                               if(!d.status){Public_Object.alert(d.msg); return;}
                               //console.log(sql);
                               Accounts_Object.reload();
                       });

                       node.dialog('close');
                   }}, */{text:"关闭",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}
                ];
                Apps.Dialog_Show(node, opt);
      },
      "change_pwd":function(){
            let node = $("#change_pwd");
            let obj = Apps.field("change_pwd",['password','new_password', 'confirm_password']);

            let opt = Apps.dialog_opt();
            opt.title =  "修改密码";
            opt.buttons = [
               {text:"确定",iconCls :'icon-ok',handler:function(){
                   let data = {
                      "password":obj.password.val(),
                      "new_password":obj.new_password.val()
                   };

                   if(!data.password){
                       Public_Object.alert("请输入原密码!");
                       return false;
                   } else if(data.new_password !== obj.confirm_password.val() || !data.new_password.length || !obj.confirm_password.val()){
                       Public_Object.alert("新密码不能为空，或两次新密码不一样!");
                       return false;
                   } else {
                       data.password = Crypt_hash(data.password,'md5');
                       data.new_password = Crypt_hash(data.new_password,'md5');
                       let dats = getByRsync(window.Api.user.change_pwd, data);
                       if(!dats.status){
                          Public_Object.alert("发生错误: " + dats.msg);
                          return false;
                       } else {  //刷新页面重新登陆
                          window.location.reload();
                       }
                   }

               }}, {text:"取消",iconCls :'icon-cancel',handler:function(){ node.dialog('close'); }}
            ];

            Apps.Dialog_Show(node, opt);
      },
      "logout": function(){
            Public_Object.confirm({"content":'确定要退出后台吗?',"title":"退出登录"}, function(index){
                   get_dats(Api.user.logout,{},function(dats, err){
                        window.location.href="/";
                   });
            });
      }
};
