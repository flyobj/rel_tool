
//列表点击样式
function list_click(){
    $(this).parent().find("li").removeClass('sel');
    $(this).addClass('sel');
    let raw = $(this).data('raw');

    let v_penal = $(".main > .center").find("> div:visible");

    let def_item = function(){
      Gui_Object.switch('tabs');
      Host_Object.list(raw.id);
    };

    if(v_penal.length){
       switch(v_penal.attr("id")){
         case "tabs": Host_Object.list(raw.id); break;
         case "accounts": Accounts_Object.list(raw.id); break;
         //case "files": Files_Object.list(raw.id); break;
         case "home": def_item(); break;
       }
    } else {
       def_item();
    }
}


/*GUI界面切换*/
let Gui_Object = {
  "panel":function(){
    return {
      "o": $(".main"),
      "left":{
        "o": $(".main > .left"),
        "sel": $(".main > .left > ul > li.sel")
      },
      "center": $(".main > .center"),
      "top":{
        "o": $(".main > .top"),
        "sel": $(".main > .top > p:first-child > a"),
        "item": function(dom){
           return this.o.find("p:first-child > a > i.iconfont"+dom).eq(0).parent();
        }
      }
    };
  },
  "lock_pencal": function(p, tb){
     let pel = this.panel();
     $(tb ? tb : "#dg").datagrid("resize");
     item = pel.top.item(p);
     if(!item.hasClass("sel")) item.addClass("sel");

     return pel.left.sel.data('raw');
  },
  "switch": function(id){
     let pel = this.panel();
     let item = null;
     let raw = null;
     pel.center.find("> div").hide();
     pel.top.sel.removeClass("sel");
     $("#"+id).show();
     switch(id){
       case "release": //代码发布
                this.lock_pencal(".icon-terminal");
                Release_Object.init();
       break;
       case "sql_task":
               this.lock_pencal(".icon-web", "#sql_dg");
               Sql_Task_Object.init();
               Sql_Task_Object.list();
       break;
       case "logs":
               this.lock_pencal(".icon-yanjing_xianshi_o", "#logs_dg");
               Logs_Object.list();
       break;
       case "accounts":
                this.lock_pencal(".icon-shezhi", "#acc_dg");
                Accounts_Object.list();
       break;
       case "projects":  //项目列表
                raw = this.lock_pencal(".icon-quanbudingdan");
                Project_Object.list();
       break;
       case "dbs":
                this.lock_pencal(".icon-shujuku", "#dbs_dg");
                Dbs_Object.list();
       break;
     }
  }
};



//业务模块
let Index_Node = {
  /*
         //退出登陆
         "logout": function(){
               Public_Object.confirm({"content":'确定要退出后台吗?',"title":"退出登录"}, function(index){
                      get_dats(Api.user.logout,{},function(dats, err){
                           window.location.href="/";
                      });
               });
         }
         */
};


/*状态通知*/
/*
let Sys_Status_Object = {
   "db": function(o){
      if(!o.status) console.log(o.error);
      $(".main > .top #db_status").css("color",o.status ? "green" : "red");
      $(".main > .top #db_status").attr("title",o.msg);
   }
};
*/

//简洁模式
function mini_model(){
      window.open(window.location.href + "#min","","width=1020,height=800,location=no,menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no");
      window.location.href="about:blank";
      window.close();
}

//初始化
function app_init(){
      if(-1 !== window.location.href.indexOf("#min")){  $("#user_menu").find("> li#um_model").remove(); }else{$("#user_menu").find("> li#um_model").click(mini_model);}
      $("#user_menu").menu({"left":"calc(100% - 110px)","top":31});
      $("#user_menu").click(function(){ $(this).hide(); });
      $("#user_info").click( function(){ $("#user_menu").show(); });

    //首页工具初始化
    Home_Tools.init();

}
