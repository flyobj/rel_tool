const Gp      = require('gulp');
const Uglify  = require('gulp-uglify');
const Babel = require('gulp-babel');
const Concat=require('gulp-concat'); //文件合并
const Usemin  = require('gulp-usemin');
const Htmlmin = require('gulp-htmlmin'); //html压缩
const Htm_Include = require('gulp-file-include'); //html文件分块合并
const cleanCSS = require('gulp-clean-css');  //css压缩
const Html_replace = require('gulp-html-replace'); //内容替换

//项目源目录
const Src  = {
  "home":"src/",
  "page":'src/page',
  "static":"src/static"
};
const Dest = {
  "home":"dest",
  "page":'dest/page',
  "static":"dest/static"
};   //目标目录

//合并, 替换, 压缩html
Gp.task('home', function () {
    let options = {
         removeComments: true,//清除HTML注释
         collapseWhitespace: true,//压缩HTML
         collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
         removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
         removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
         removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
         minifyJS: true,//压缩页面JS
         minifyCSS: true//压缩页面CSS
     };
     let tp = (new Date()).valueOf();
     let htm_rep = {
             css: {src: tp, tpl: '<link rel="stylesheet" href="./static/css/css.css?t=%s">'},
             js:{src: tp, tpl: '<script type="text/javascript" src="./static/js/all.js?t=%s"></script>'}
       };
    return Gp.src(Src.page + "/index.html").pipe(Htm_Include({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(Html_replace(htm_rep))
        .pipe(Htmlmin(options))
        .pipe(Gp.dest(Dest.home));
});

//复制图片字体资源,合并js, 合并压缩css
Gp.task("static", function(){
  let css = Src.static + "/style/";
  let js = Src.static + "/js/";
  let jsl = Src.static + "/lib/";
  let css_list = [
     css+"global.css",
     css+"jquery-ui.min.css",
     css+"easyui.css",
     css+"index.css",
     css+"iconfont.css",
     css+"icon.css",
     css+"login.css",
     css+"account.css",
     css+"sql_task.css",
     css+"logs.css",
     css+"main.css",
     css+"me.css",
     css+"dbs.css"
   ];

   let js_list = [
       jsl+"jquery.min.js",
       jsl+"jquery-ui.min.js",
       jsl+"jquery-qrcode.min.js",
       jsl+"jquery.easyui.min.js",
       jsl+"easyui-lang-zh_CN.js",
       js+"app.js",
       js+"hash.js",
       js+"func.js",
       js+"public_object.js",
       js+"login.js",
       js+"project_object.js",
       js+"accounts_object.js",
       js+"sql_task_object.js",
       js+"release_object.js",
       js+"logs_object.js",
       js+"home_tools.js",
       js+"index.js",
       js+"main.js",
       js+"me.js",
       js+"dbs_object.js"
   ];

  Gp.src(Src.static + "/img/**/*").pipe(Gp.dest(Dest.static + "/img/"));
  Gp.src(Src.static + "/codemirror/**/*").pipe(Gp.dest(Dest.static + "/codemirror/"));
  Gp.src(Src.static + "/css/**/*").pipe(Gp.dest(Dest.static + "/css/"));
  Gp.src(css_list).pipe(Concat('css.css')).pipe(cleanCSS()).pipe(Gp.dest(Dest.static + "/css/"));
  return Gp.src(js_list).pipe(Concat('all.js'))
  //.pipe(Babel({presets: ['@babel/env']})).pipe(Uglify())
  .pipe(Gp.dest(Dest.static + "/js/"));
});


Gp.task('default', Gp.series('static','home'));
